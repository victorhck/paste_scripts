Este repositorio contiene unas versiones modificadas por mí de los scripts _susepaste_ y _susepaste-screenshot_ que ofrece openSUSE en `/usr/bin/` y que están disponibles en este repositorio de openSUSE:
* [https://github.com/openSUSE/paste/tree/master/script](https://github.com/openSUSE/paste/tree/master/script)

Los scripts sirven para subir un texto o una imagen de nuestro equipo al [servicio paste de openSUSE](https://susepaste.org)

## Modificaciones realizadas:
* Añadir en la opción de expiración un diálogo más amigable basado en minutos, horas, días, meses, años:

`30m = 30 minutos; 1h = 1 hora; 6h = 6 horas; 12h = 12 horas; 1d = 1días; 1w = 1 semana; 1m = 1 mes; 3m = 3 meses; 1y = 1 año; 2y = 2 años; 3y = 3 años; 0 = nunca`
* Añadir la opción -d [seg] para añadir un _delay_ a la hora de tomar una captura de pantalla con _susepaste-screenshot_.
* Solucionar error al copiar el texto al portapapeles del sistema

## Instalación

* Renombra los archivos susepaste y susepaste-screenshot en la ruta `/usr/bin` con otro nombre (por ejemplo añade -orig al nombre) para poder volver atrás si no te convence cómo funcionan estos scripts
* Descarga los scripts mediante:
** `wget https://codeberg.org/victorhck/paste_scripts/raw/branch/main/susepaste`
** `wget https://codeberg.org/victorhck/paste_scripts/raw/branch/main/susepaste-screenshot`
* Como usuario root pégalos en la ruta anterior (o en una ruta que tu prefieras de tu _path_ por ejemplo `/usr/local/bin/`
** `sudo mv susepaste* /usr/bin/`

Puedes descargar también el archivo `lang-mappings` que servirá junto con el comando `file` para determinar automáticamente el lenguaje del archivo que quieres subir.

Ubica este archivo en `/usr/share/` o renombra el archivo y ubícalo en `~/.susepaste_lang_mappings.sed`

## Módo de utilización

### susepaste

`susepaste [-n nick] [-t título] [-e expira] [-f <archivo> o <ruta/al/archivo>]`

(expira: 30m=30minutos; 1h=1hora; 6h=6horas; 12h=12horas; 1d=1día; 1w=1semana; 1m=1mes; 3m=3meses; 1y=1año; 2y=2años; 3y=3años; 0=nunca)

Este script sirve para subir texto o un archivo al servicio de [paste de openSUSE](https://susepaste.org/). Podemos subir un texto que vayamos introduciendo y que para finalizar lo hagamos con `Ctrl+D`, podemos subir el resultado de un comando mediante una tubería `|`, o podemos subir un archivo de texto

Podemos especificar o no, los campos de: nombre de usuario, título, tiempo de expiración o especificar un fichero. Si no se especifica nada, utilizará unos valores predeterminados.

__Ejemplos__

Subir un archivo especificando mi nick, el título, que dure 1 semana en el servidor y el archivo a subir:

`susepaste -n Victorhck -t script_bash -e 1w -f ejemplo.sh`

Subir el resultado de un comando sin especificar nada más:

`uname -a | susepaste`

### susepaste-screenshot

`susepaste-screenshot [--all] [-n nick] [-t título] [-e expira] [-d retraso en segundos] [-f <archivo> o <ruta/al/archivo>]`

(expira: 30m=30minutos; 1h=1hora; 6h=6horas; 12h=12horas; 1d=1día; 1w=1semana; 1m=1mes; 3m=3meses; 1y=1año; 2y=2años; 3y=3años; 0=nunca)

Este script sirve para subir una captura de pantalla que hagamos de nuestro equipo o desde un archivo de imagen al servicio de [paste de openSUSE](https://susepaste.org/)

Podemos especificar o no los campos de: nombre de usuario, título, tiempo de expiración, retraso en la toma de la captura o especificar la ruta de un fichero de una imagen:

__Ejemplos__

Subir una captura de nuestro equipo especificando el nick, el título, que dure 1 mes en el servidor y que espere 5 segundos a realizar la captura:

`susepaste-screenshot -n Victorhck -t mi_captura -e 1m -d 5`

Subir un archivo con una imagen especificando que dure tres semanas en el servidor:

`susepaste-screenshot -e 3m -f /home/mi_usuario/capturas/mi_captura.png`

## Contribuye

Se agradecen comentarios
